
/* //обов'язкова частина
const user1 = createNewUser();
function createNewUser( ){
    const first_Name = prompt("Уведіть своє ім'я: ",'name');
    const last_Name = prompt("Уведіть своє прізвище: ",'last name');
    const newUser = {
        firstName: first_Name,
        lastName: last_Name,
        getLogin(){
            return  this.firstName[0] + this.lastName.toLowerCase();

        },

    };
    return newUser;
}

console.log(user1.getLogin());
*/




/*з використанням функцій сетерів*/
function createNewUser( ){
    // const first_Name = prompt("Уведіть своє ім'я: ",'name');
    // const last_Name = prompt("Уведіть своє прізвище: ",'last name');
    const newUser = {
        firstName: prompt("Уведіть своє ім'я: ",'name'),
        lastName: prompt("Уведіть своє прізвище: ",'last name'),
        getLogin(){
            return  this.firstName[0] + this.lastName.toLowerCase();

        },

        setFirstName: function (first_Name_) {
            Object.defineProperty(newUser, 'firstName', {
                value: first_Name_
            });

        },

        setLastName: function(last_Name_){
            Object.defineProperty(newUser, 'lastName', {
                value: last_Name_
            });

        }

    };

    Object.defineProperty(newUser, 'firstName', {
        writable: false

    });

    Object.defineProperty(newUser, 'lastName', {
        writable: false

    });
    // let descriptor = Object.getOwnPropertyDescriptor(newUser, 'firstName');
    // console.log(descriptor);
    // /* перегляд повної інформації про властивість firstName об'єкта newUser - використовується метод getOwnPropertyDescriptor */
    return newUser;
}
const user1 = createNewUser();

console.log(user1);// ---- тут виводиться об'єкт newUser ---------;

console.log(user1.getLogin()); //---- тут результат роботи методу getLogin()-----

user1.firstName = 'Vasilij';
user1.lastName = 'Pupkin';
console.log(user1);/*---- тут спроба переприсвоїти значення властивостям firstName, lastName та
 виведення обєкта з 'новими ' значеннями властивостей -----*/


console.log('-------- а ось об"єкт після перезаписування значеннь через ф-ції сертери setFirstName,  setLastName ----');
user1.setFirstName('ahgd');
user1.setLastName('ytwtdytq');
console.log(user1);// ---- а ось перезаписування значеннь через ф-ції сертери setFirstName,  setLastName ------