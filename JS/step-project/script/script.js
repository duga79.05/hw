// використання tab для перемикання вкладок в блоці Our Services

const tabsMenu = Array.from(document.getElementsByClassName('tabs'));
const tabsContent = Array.from(document.getElementsByClassName('tabs-panel'));


document.getElementsByClassName('tabs_')[0].addEventListener('click',
    function (e) {
        tabsMenu.forEach(item => item.classList.remove('active'));
        if (e.target.classList.contains("tabs")) {
            e.target.classList.add('active');
            tabsContent.forEach((item) => {
                item.classList.remove('active');
                if (item.dataset.tabs === e.target.dataset.tabs) item.classList.add('active')
            })

        }
        console.log(tabsContent);
    });



// Our Amazing Work

filterSelection("all");// визов ф-ції filterSelection, за замовчуванням параметр "all"
function filterSelection(nameClass) {
    const alreadyShowedClass = document.querySelectorAll(".column.show"); // 12 || 24
    alreadyShowedClass.forEach(function (item) {
        if (nameClass != 'all') {
            if (!item.classList.contains(nameClass)) {
                item.style.display = 'none';
            } else {
                item.style.display = 'block';

            }
        } else {
            item.style.display = 'block';

        }

    });

}


// Додаю клас active до поточної .btn кнопки-меню

const btns = Array.from(document.getElementsByClassName("btn"));
const imgBox = Array.from(document.getElementsByClassName("column"));

document.getElementById("myBtnContainer").addEventListener('click',
    function (e) {
        btns.forEach(item => item.classList.remove('active'));
        if (e.target.classList.contains("btn")) {
            e.target.classList.add('active');
        }
    });


// тепер відпрацьовую натискання на кнопку Load More

$(function () {
    $(".column").slice(0, 12).show();// вказую к-сть видимих елементів
    $(".btn-load").on('click', function (e) {
        e.preventDefault();
        $(".pre-load-wrapp").removeClass('hidden');

        setTimeout(function () {
            $('.column').slideDown('slow').addClass('show');
            let currentActiveBtn = $('.btn.active')[0].innerText.split(' ')[0].toLowerCase();
            filterSelection(currentActiveBtn);

            if ($(".column.show").length === 24) {
                // console.log($(".column.show").length);
                $(".btn-load").remove();
                $(".pre-load-wrapp").addClass('hidden');
            }

        }, 5000);

    });
});

//slider
$('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: true,
    centerMode: true,
    focusOnSelect: true
});




/*Попередній некоректно працюючий код фільтра і підвантажувальної кнопки - неправильно працюють разом, але окремо - робочі*/

//
// // Our Amazing Work
// filterSelection("all");// визов ф-ції filterSelection, за замовчуванням параметр "all"
//
// // створюю ф-цію filterSelection, відповідає чи потрібно відображати елемент з відповідним класом
// function filterSelection(nameClass) {
//     const showClass = document.getElementsByClassName("column");// звернення до всіх ел-тів класу "column"
//     if (nameClass === "all") nameClass = ""; // перевірка на вміст параметра nameClass
//
//     showClass.length;// тут треба щось робити з довжиною!!!!!
//
//     for (let i = 0; i < 12; i++) {// перебір всіх елементів класу "column"
//         RemoveClass(showClass[i], "show");// видалення у всіх елементів класу  "show",
//         // ф-ція RemoveClass описана нижче
//         if (showClass[i].className.indexOf(nameClass) > -1) AddClass(showClass[i], "show");
//         // за умови знаходженні елемента з відповідним className (indexOf(nameClass), повертає позицію елемента,
//         // якщо знаходить його. Якщо ні, то повертає -1) - додається клас "show", за допомогою функції AddClass,
//         // описана  нижче
//     }
// }
//
// // ф-ція для відображення відфільтрованих елементів
// function AddClass(element, name) {
//     const arr1 = element.className.split(" ");// створюю масив з усіх класів відповідного елементу, використовуючи
//     // метод split з параметром " "
//     const arr2 = name.split(" ");// створюю масив елементів, які мають відповідне ім'я класу,
//
//     for (let i = 0; i < arr2.length; i++) {
//         if (arr1.indexOf(arr2[i]) === -1) {// як правильно прочитати цей рядок ????!!!!!!
//             element.className += " " + arr2[i];
//         }
//     }
// }
//
// //ф-ція приховує не вибрані ел-ти
// function RemoveClass(element, name) {
//     const arr1 = element.className.split(" ");
//     const arr2 = name.split(" ");
//     for (let i = 0; i < arr2.length; i++) {
//         while (arr1.indexOf(arr2[i]) > -1) {// ????!!!!!!!!!
//             arr1.splice(arr1.indexOf(arr2[i]), 1);
//         }
//     }
//     element.className = arr1.join(" ");// повертає імена класів елемента, що були задані як масив, тепер вже у вигляді
//     //  рядка, записаних через " " . За це відповідає метод join(" ")
// }
//
//
// // Додаю клас active до поточної .btn кнопки-меню
//
// const btns = Array.from(document.getElementsByClassName("btn"));
// const imgBox = Array.from(document.getElementsByClassName("column"));
//
// document.getElementById("myBtnContainer").addEventListener('click',
//     function (e) {
//         btns.forEach(item => item.classList.remove('active'));
//         if (e.target.classList.contains("btn")) {
//             e.target.classList.add('active');
//         }
//     });
//
//
// // тепер відпрацьовую натискання на кнопку Load More
//
// $(function () {
//     $(".column").slice(0, 12).show();// вказую к-сть видимих елементів
//     $(".btn-load").on('click', function (e) {
//         e.preventDefault();
//         $(".column:hidden").slice(0, 12).slideDown();// к-сть добавлених ел-тів при кліку
//         if ($(".column:hidden").length === 0) {
//             $("load").fadeOut('slow');
//             $(".btn-load").remove();
//
//         }
//
//     });
// });
//

