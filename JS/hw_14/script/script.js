
// const $tabsMenu = $('.tabs');
// const $tabsContent = $('.tabs-panel');

$(document).ready(function() {// запускає функцію прописану всередині

    $('.tabs').on('click', function() {// звернення до всіх li class="tabs"
        console.log(this);
        $(this)
            .addClass('active')//додаю до вибраного елемента батьківського блоку клас active
            .siblings()//повертає всі елементи братів вибраного елемента.
            // Дерево DOM: Цей метод рухається вперед і назад вздовж побратимів елементів DOM.
            .removeClass('active')//видаляю клас  active для всіх елементів, крім вибраного
            .closest('.centered-content') // метод JQuery , який повертає першого предка обраного елемента в дереві DOM.
            // Цей метод переходить вгору від поточного елемента в пошуках першого предка елемента
            .find('.tabs-panel')// відшуковує всі елементи заданого класу
            .removeClass('active')// видаляємо тепер клас active у свіх елементів з класом tabs-panel
            .eq($(this).index())//вибірка елемента по індексу
            .addClass('active')// додаю клас active до вибраного
    })
});




















