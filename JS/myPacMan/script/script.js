/*ЗАДАНИЕ - 5 дорабатываем пэкмэна
* Реализовать несколько режимов шага пэкмэна, показывать эту справку пользователю всегда в правом верхнем углу и
*  подсвечивать синим цветом активный режим:
*   1 - одно нажатие = сдвиг на 25рх. Включается с помощью сочетания клавиш Ctrl+1
*   2 - одно нажатие = сдвиг на  50рх Включается с помощью сочетания клавиш Ctrl+2
*   3 - одно нажатие = сдвиг на 100px Включается с помощью сочетания клавиш Ctrl+3
* Разворачивать пэкмэна в зависимости от направления движения.
* Проверять не вышел ли пэкмэн за границы поля.
*/

let myPacMan = document.querySelector('.circle');
const panelText = document.createElement('div');
// тут добавляю панель
const array = [" - одно нажатие = сдвиг на 25рх. Включается с помощью сочетания клавиш Ctrl+1",
    " - одно нажатие = сдвиг на  50рх Включается с помощью сочетания клавиш Ctrl+2",
    " - одно нажатие = сдвиг на 100px Включается с помощью сочетания клавиш Ctrl+3"];

let ol = document.createElement('ol');
array.forEach(function (elem, index) {
    let li = document.createElement('li');
    li.innerHTML = elem;


    let result = ol.append(li);
    ol.style.cssText = "position: absolute; top: 5px; right: 5px; text-align: right;" +
        " color: black; size:16px; background-color: yellow";

});
document.body.append(ol);
//backLight1 для підсвітки, коли спрацює умова Ctrl+1
const backLight1 = document.querySelector('li:first-child');
//backLight1 для підсвітки, коли спрацює умова Ctrl+3

const backLight3 = document.querySelector('li:last-child');
//backLight1 для підсвітки, коли спрацює умова Ctrl+2
const backLight2 = document.querySelector('li:nth-of-type(2)');
let step = 20;
document.addEventListener('keydown', (e) => {
    if ((e.ctrlKey && e.code === "Digit1") || (e.ctrlKey && e.code === "Numpad1")) {
        // тут повинна спрацювати підсвідка
        backLight3.style.cssText = 'background-color:yellow;';
        backLight2.style.cssText = 'background-color:yellow;';
        backLight1.style.cssText = 'background-color:blue;';
        return step = 25;
    }
    // else backLight1.style.cssText = 'background-color:yellow';

    if ((e.ctrlKey && e.code === "Digit2") || (e.ctrlKey && e.code === "Numpad2")) {
        backLight3.style.cssText = 'background-color:yellow;';
        backLight1.style.cssText = 'background-color:yellow;';
        backLight2.style.cssText = 'background-color:blue;';
        //
        // document.querySelectorAll('li:nth-of-type(2)').style.backgroundColor = 'orange';
        return step = 50;
    }
    if ((e.ctrlKey && e.code === "Digit3") || (e.ctrlKey && e.code === "Numpad3")) {
        backLight2.style.cssText = 'background-color:yellow;';
        backLight1.style.cssText = 'background-color:yellow;';
        backLight3.style.cssText = 'background-color:blue;';
        //
        // document.querySelectorAll('li:last-child').style.backgroundColor = 'orange';
        return step = 100;
    }

    console.log(e.code);
    console.log(step);
    let coords = myPacMan.getBoundingClientRect();
    switch (e.key) {
        case "ArrowUp": {
            // debugger;

            myPacMan.style.top = coords.top - step + "px";
            myPacMan.style.transform = "rotate(-90deg)";
            console.log(step);
            break;
        }

        case "ArrowDown": {


            myPacMan.style.top = coords.top + step + "px";
            myPacMan.style.transform = "rotate(90deg)";
            console.log(step);

            break;
        }

        case "ArrowLeft": {

            myPacMan.style.left = coords.left - step + "px";
            myPacMan.style.transform = "rotate(180deg)";
            myPacMan.style.transform = "scale(-1, 1)";
                console.log(step);
            break;
        }
        case "ArrowRight": {

            myPacMan.style.left = coords.left + step + "px";
            myPacMan.style.transform = "rotate(0deg)";

            break;
        }
    }


});




