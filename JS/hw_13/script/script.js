const topicBtn = document.getElementsByClassName('topic')[0];
const styleToggle = document.getElementById('style');

styleToggle.setAttribute('href', localStorage.getItem('theme'));// звертаюся до атрибуту 'href'
// і записую, що туди класти, а кладу значення ключа theme з  localStorage.
// Це значення запишеться після того як спрацює один із case

if(!localStorage.getItem('theme')){// тут відбівається перевірка чи не порожнє значення ключа 'theme
    //  в localStorage і якщо порожне то запише наступне :
    localStorage.setItem('theme', '../css/style3.css')
}
topicBtn.addEventListener('click', (e) => {// відслідковується подія натискання на кнопку
const atr = styleToggle.getAttribute('href');// резервую змінну atr для запису значень атрибуту href,
    console.log(atr);
    switch (atr) {// виконую перевірку вмісту атрибуту href
        case '../css/style3.css':
            styleToggle.setAttribute('href', '../css/style-black.css');
            console.log(atr);
            localStorage.setItem('theme', '../css/style-black.css');// записую значення ключа по атрибуту
            break;

        case '../css/style-black.css':
            styleToggle.setAttribute('href', '../css/style3.css');
            console.log(atr);
            localStorage.setItem('theme', '../css/style3.css');
            break;
    }
});


//localStorage.setItem('name','Irka');// добавляю або змінюю значення в localStorage ("ключ", "значення")
// const localValue = localStorage.getItem('name');// отримую знання з localStorage
// console.log(localValue);// виводжу значення в консоль
// localStorage.removeItem('name');//видалення запису по ключу
// localStorage.removeItem('userName');
//localStorage.clear()//очищення всього сховища