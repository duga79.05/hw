const slides = document.querySelectorAll('.images-wrapper .image-to-show');// отримуємо всі слайди з контейнера
let index = 0;//створюю змінну для отримання поточного слайда
let slideInterval = setInterval(nextSlide, 2000);

function nextSlide() {
    slides[index].className = 'image-to-show';//міняю клас для поточного слайда, щоб заховати його.
    // Властивість transition в css автоматично обробляє плавне загасання
    index = (index + 1) % slides.length;//додаю клас до поточного слайда.
    //  використовую оператор % на випадок, якщо це був останній слайд, щоб повернутися до першого
    slides[index].className = 'image-to-show showing';//Після отримання індексу слайда міняю клас і показую новий.
    // І знову прозорість обробляється властивістю transition.
}


let play = true;
const pauseButton = document.getElementById('pause');
const goButton = document.getElementById('go');


function pauseSlideshow() {
    play = false;
    clearInterval(slideInterval);
}

function playSlideshow() {
    play = true;
    slideInterval = setInterval(nextSlide, 2000);
}

pauseButton.onclick = function () {
    if (play) pauseSlideshow();
};

goButton.onclick = function () {
    if (!play) playSlideshow();
};

