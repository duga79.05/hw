## Теоретичне питання

1. Опишіть своїми словами як працює цикл forEach.

## Відповідь

1. Цикл forEach використовується при переборі елементів масиву та виконання над ними певних дій.
 Цей метод відноситься до _функцій вищого порядку_.
 Містить в собі кол-бек функцію (ф-цію зворотнього зв'язку), яка в свою чергу має 3-и параметри: 
 безпосередньо елемент масиву, індекс та сам масив.
 Залежно від примінення всі ці параметри вказувати необов'язково, інколи достатньо вказати лише елемент, як в д/з :-).
 2. Особливістю цього методу є те, що він не повертає ніякого значення. 
 нпр.,
                const newArr4 = [];
              array.forEach((element, index, array) => {
                  if (typeof element !== 'undefined') {
                      // console.log(element);
                      newArr4.push(element);
                  }
  
  тут відбувається перебір елементів масиву array, значення якого вказані як параметр зовнішної ф-ції, перевіряється
  умова і при виконанні умови дані елементи записуються в новий масив newArr4 методом push.
   Сама функція нічого не повертає! 
  