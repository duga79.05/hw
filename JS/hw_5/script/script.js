
function createNewUser( ){

    const newUser = {
        firstName: prompt("Уведіть своє ім'я: ",'name'),
        lastName: prompt("Уведіть своє прізвище: ",'last name'),
        birthday:  prompt("Уведіть дату народження: ",'dd.mm.yyyy'),

        getAge (){
            const date = this.birthday.split(".");
            const birthday = new Date (` ${date[2]}.${date[1]}.${date[0]}`);
            const today = new Date();
            const age = today.getFullYear() - birthday.getFullYear();
            //перевірка скільки виповнилося повних років користувачу
if (today.getMonth() > birthday.getMonth() ||
    (today.getMonth() === birthday.getMonth() && today.getDate() >= birthday.getDate()))
            return age;
else return age - 1;

        },
        getPassword (){
            const date = this.birthday.split(".");
            const pass = this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + date[2];
            return pass;
        },
       getLogin(){
            return  this.firstName[0] + this.lastName.toLowerCase();

        },

        setFirstName: function (first_Name_) {
            Object.defineProperty(newUser, 'firstName', {
                 value: first_Name_
            });

        },

        setLastName: function(last_Name_){
            Object.defineProperty(newUser, 'lastName', {
                  value: last_Name_
            });

        }

    };

    Object.defineProperty(newUser, 'firstName', {
        writable: false

    });

    Object.defineProperty(newUser, 'lastName', {
        writable: false

    });
        // let descriptor = Object.getOwnPropertyDescriptor(newUser, 'firstName');

    // console.log(descriptor);
    // /* перегляд повної інформації про властивість firstName об'єкта newUser - використовується метод getOwnPropertyDescriptor */
    return newUser;
}
const user1 = createNewUser();

console.log(`Ваш вік: ${user1.getAge()} років`);
console.log(`Login: ${user1.getLogin()}`); //---- тут результат роботи методу getLogin()-----
console.log(`Ваш пароль: ${user1.getPassword()}`);



