const input = document.forms['inputForm'];
const inputArea = document.getElementsByName('price')[0];
const span = document.createElement("span");
const button = document.createElement("img");
const redText = document.createElement("p");
redText.innerText = "Please enter correct price";
input.classList.add("input-close");
input.addEventListener("focus", () => {
        inputArea.style.cssText = " border: green 2px solid; outline: none;"
    },
    true);
input.addEventListener("blur", () => {
    inputArea.style.cssText = " border:grey solid 1px;";
    if (inputArea.value >= 0) {
         creatSpan();
    } else {
        redContent();
        inputArea.style.cssText = "border: solid red 2px;";
        if (document.body.querySelector("span")) {
            clear();
        }
    }
}, true);

function redContent() {
    document.body.appendChild(redText);
}

function creatSpan() {
    span.innerText = `Поточна ціна: ${inputArea.value}`;
    span.classList.add('top-span');
    button.src = "img/close.png";
    button.classList.add('close');
    document.body.appendChild(span);
    document.body.appendChild(button);


    if (document.body.querySelector("p")) {
        document.body.removeChild(redText);

    }
}

button.onclick = function () {
    clear();
};


function clear() {
    inputArea.value = null;
    inputArea.classList.remove('input-close');
    document.body.removeChild(span);
    document.body.removeChild(button);
}
