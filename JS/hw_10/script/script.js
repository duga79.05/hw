const passwordForm = document.forms['passwordForm'];
const inputPassword = document.querySelector('#password');
const inputVerification = document.querySelector('#verification');
const submitBtn = document.querySelector('.btn');
const span = document.createElement('span');
const label = passwordForm.querySelector('.input-wrapper');

let hidden = passwordForm.querySelector('.fa-eye-slash');
let visible = passwordForm.querySelector('.fa-eye');

inputVerification.type = 'text';
inputPassword.type = 'password';

hidden.addEventListener('click', (e) => {
    if (e.target.classList.contains('fa-eye-slash')) {
        hidden.classList.remove('fa-eye-slash');
        hidden.classList.add('fa-eye');
        inputPassword.type = 'text';
    } else {
        hidden.classList.remove('fa-eye');
        hidden.classList.add('fa-eye-slash');
        inputPassword.type = 'password';

    }
});
visible.addEventListener('click', (e) => {
    if (e.target.classList.contains('fa-eye-slash')) {
        visible.classList.remove('fa-eye-slash');
        visible.classList.add('fa-eye');
        inputVerification.type = 'text';
    } else {
        visible.classList.remove('fa-eye');
        visible.classList.add('fa-eye-slash');
        inputVerification.type = 'password';

    }
});

// const passwordAttr = inputPassword.getAttribute('type');

submitBtn.addEventListener('click', (e) => {
    if (inputPassword.value === inputVerification.value) {
        span.remove();
         setTimeout(() => alert("You are welcome!"),300);

    } else {
        creatSpan();
    }
});

function creatSpan() {
    span.innerText = 'Потрібно ввести однакові значенія!!!';
    submitBtn.before(span);
}
